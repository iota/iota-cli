package toolkit

import (
	"fmt"
	"github.com/guonaihong/gout"
	"time"
)

const TimeOut = time.Second * 5

var defaultHeader = gout.H{
	"Accept":     "application/json",
	"User-Agent": "IOTA/beta",
}

type RepoCommits []*RepoCommit

type RepoCommit struct {
	Author struct {
		Active            bool      `json:"active"`
		AvatarURL         string    `json:"avatar_url"`
		Created           time.Time `json:"created"`
		Description       string    `json:"description"`
		Email             string    `json:"email"`
		FollowersCount    int       `json:"followers_count"`
		FollowingCount    int       `json:"following_count"`
		FullName          string    `json:"full_name"`
		ID                int       `json:"id"`
		IsAdmin           bool      `json:"is_admin"`
		Language          string    `json:"language"`
		LastLogin         time.Time `json:"last_login"`
		Location          string    `json:"location"`
		Login             string    `json:"login"`
		ProhibitLogin     bool      `json:"prohibit_login"`
		Restricted        bool      `json:"restricted"`
		StarredReposCount int       `json:"starred_repos_count"`
		Visibility        string    `json:"visibility"`
		Website           string    `json:"website"`
	} `json:"author"`
	Commit struct {
		Author struct {
			Date  string `json:"date"`
			Email string `json:"email"`
			Name  string `json:"name"`
		} `json:"author"`
		Committer struct {
			Date  string `json:"date"`
			Email string `json:"email"`
			Name  string `json:"name"`
		} `json:"committer"`
		Message string `json:"message"`
		Tree    struct {
			Created time.Time `json:"created"`
			Sha     string    `json:"sha"`
			URL     string    `json:"url"`
		} `json:"tree"`
		URL string `json:"url"`
	} `json:"commit"`
	Committer struct {
		Active            bool      `json:"active"`
		AvatarURL         string    `json:"avatar_url"`
		Created           time.Time `json:"created"`
		Description       string    `json:"description"`
		Email             string    `json:"email"`
		FollowersCount    int       `json:"followers_count"`
		FollowingCount    int       `json:"following_count"`
		FullName          string    `json:"full_name"`
		ID                int       `json:"id"`
		IsAdmin           bool      `json:"is_admin"`
		Language          string    `json:"language"`
		LastLogin         time.Time `json:"last_login"`
		Location          string    `json:"location"`
		Login             string    `json:"login"`
		ProhibitLogin     bool      `json:"prohibit_login"`
		Restricted        bool      `json:"restricted"`
		StarredReposCount int       `json:"starred_repos_count"`
		Visibility        string    `json:"visibility"`
		Website           string    `json:"website"`
	} `json:"committer"`
	Created time.Time `json:"created"`
	Files   []struct {
		Filename string `json:"filename"`
	} `json:"files"`
	HTMLURL string `json:"html_url"`
	Parents []struct {
		Created time.Time `json:"created"`
		Sha     string    `json:"sha"`
		URL     string    `json:"url"`
	} `json:"parents"`
	Sha string `json:"sha"`
	URL string `json:"url"`
}

// GetRepoLatestCommit 获取仓库最新提交记录
func GetRepoLatestCommit() (*RepoCommit, error) {
	var commits RepoCommits

	api := fmt.Sprintf("https://gitea.com/api/v1/repos/iota/iota-cli/commits")

	if err := gout.GET(api).SetHeader(defaultHeader).SetQuery(gout.H{
		"page":  1,
		"limit": 5,
	}).SetTimeout(TimeOut).BindJSON(&commits).Do(); err != nil {
		return nil, err
	}

	if len(commits) == 0 {
		return nil, fmt.Errorf("empty commit")
	}
	return commits[0], nil
}
