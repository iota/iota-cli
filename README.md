### iota-cli

`iota-cli`命令的详细介绍

### 安装

```shell
$ go install gitea.com/iota/iota-cli@latest
## output
go: downloading gitea.com/iota/iota-cli v0.0.0-20210902040340-7af67c79f914
```

安装完毕后执行 `iota-cli -h` 验证是否安装成功

```shell
[x@fedora ~]$ iota-cli 
iota-cli 是基于 iota 库的一个提高生产效率的工具链

Usage:
  iota-cli [command]

Available Commands:
  addapi      给路由组新增一个api
  addroute    快速添加一个路由组
  addrpc      给service新增一个rpc
  addsvc      快速添加一个rpc服务
  completion  Generate the autocompletion script for the specified shell
  create      创建一个新项目
  dep         更新iota-cli依赖的工具链
  fmt         格式化 proto 文件使其看的赏心悦目
  gen         解析proto文件, 自动生成开发代码.
  help        Help about any command
  md          给路由组的api输出markdown文档
  run         快速运行项目
  update      检测并更新iota-cli
  version     打印iota-cli版本信息

Flags:
  -h, --help   help for iota-cli

Use "iota-cli [command] --help" for more information about a command.

```

### iota-cli 版本更新

在安装完 `iota-cli` 后, 可以使用内置的 `update` 命令进行自更新. 当然, builder 内置有检测是否有新版本的机制,当版本过低,将提醒你手动更新

```shell
[x@fedora ~]$ iota-cli update
-----
更新 iota-cli 插件中...

iota-cli 更新完成
-----

```

### iota-cli 依赖工具链更新

在安装完 `iota-cli` 后, 务必使用内置的 `dep` 命令进行工具链更新.

```shell
[x@fedora ~]$ iota-cli dep
检测 protoc 插件中...
protoc local version: v3.18.0
protoc latest version: v3.18.1
正在下载 protoc...
插件将被放置到: /home/x/go/bin/protoc
include文件夹将被放置在: /home/x/go/bin/include
protoc version: libprotoc 3.18.1
安装 protoc完毕
-----
更新 protoc-go-inject-tag 插件中...
protoc-go-inject-tag更新完成
-----
更新 protoc-gen-go 插件中...
protoc-gen-go更新完成
-----
更新 protoc-gen-go-grpc 插件中...

protoc-gen-go-grpc 更新完成
-----
-----
更新 iota-cli 插件中...

iota-cli 更新完成
-----
```

### 新建项目

在当前目录新建一个名为 `MyProject` 的项目

```shell
[x@fedora ~]$ iota-cli create --name MyProject --path .
```
