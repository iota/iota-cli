package template

const templateConfig = `package config

import (
	"fmt"
	"gitea.com/iota/iota/core"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"os"
)

var conf *ServerConf

type ServerConf struct {
	ConfigFile		string
	core.ServerConfig ` + "`" + `yaml:",inline"` + "`" + `
}

func New(fileName string) {
	conf = new(ServerConf)
	conf.ConfigFile = fileName
}

func Get() *ServerConf {
	if conf == nil {
		panic(fmt.Errorf("config not init"))
	}
	return conf
}

func (receiver *ServerConf) Load() error {
	f, err := os.Open(receiver.ConfigFile)
	if err != nil {
		return err
	}
	defer f.Close()
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(data, receiver); err != nil {
		return err
	}
	return nil
}
`
