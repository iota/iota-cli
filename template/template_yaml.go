package template

const templateLocalYaml = `server-name: {{.Name}}
server-listen: 0.0.0.0:12580
environment: local
`

const templateDevYaml = `server-name: {{.Name}}
server-listen: 0.0.0.0:12580
environment: dev
`

const templateProdYaml = `server-name: {{.Name}}
server-listen: 0.0.0.0:12580
environment: prod
`
