package template

const templateDemoModuleProtoDefine = `syntax = "proto3";

package controller;

option go_package = "{{.Name}}/internal/controller";


// @route_group: true
// @route_api: /api/test
// @gen_to: ./internal/controller/demo_controller.go
service DemoAPI {
    // @desc: echo
    // @author: 匿名
    // @method: POST
    // @api: /ping
    rpc Ping (PingReq) returns (PingResp);
}


message PingReq {
    string ping = 1; // 输入字符串 我会将该字符串回显给你
}

message PingResp {
    string pong = 1; // 将输入的字符串原样输出
}
`

const templateServices = `package services

import "context"

type Test struct {
	Appid     string 
}

func (test *Test) Test(c context.Context)  error {
	return nil
}
`
const templateLogic = `package logic

import "context"

type Test struct {
	Appid     string 
}

func (test *Test) Test(c context.Context)  error {
	return nil
}
`
