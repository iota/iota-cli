package template

const templateRouter = `package router

import (
	"fmt"
	"gitea.com/iota/iota/core"
	"github.com/gin-gonic/gin"
	"os"
	"{{.Name}}/config"
	"{{.Name}}/infra/middleware"
)

type Router struct{}

func NewRouter() *Router {
	return &Router{}
}

// Register 注册路由
func (receiver *Router) Register() error {
	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery(), middleware.CorsFilter())

	// 从这里开始实例化路由注册器
	_ = core.NewRegister()

	cfg := config.Get()
	_, _ = fmt.Fprintf(os.Stdout, "api server: %s\n", cfg.ServerListen)
	return router.Run(cfg.ServerListen)
}

// Init 注册前的路由初始化
func (receiver Router) Init() error {
	return nil
}
`

const templateMiddleware = `package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// CorsFilter 跨域过滤器
func CorsFilter() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Authorization,X-CSRF-Token,Token,session")
		c.Header("Access-Control-Allow-Methods", "*")
		c.Header("Access-Control-Expose-Headers", "*")
		if c.Request.Method == "OPTIONS" {
			c.String(http.StatusNoContent, "OPTIONS")
			return
		}
	}
}

// ResponseJsonHeader 统一设置返回json格式数据
func ResponseJsonHeader() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Content-Type", "application/json")
	}
}
`
