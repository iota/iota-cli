module gitea.com/iota/iota-cli

go 1.16

require (
	gitea.com/iota/toolkit v0.0.0-20211216085837-d28478c33804
	github.com/elliotchance/pie v1.39.0
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/guonaihong/gout v0.2.10
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
